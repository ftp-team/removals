class Pdata
  def initialize(dbh)
    $LOG.debug("Initializing Pdata object")
    @data = Hash.new
    @helper = Array.new
    @faked = Array.new
    @db = dbh
  end

  def binary_rows()
    text = ""
    @helper.uniq!
    @helper.each do |p|
      $LOG.debug("Looking at row of package #{p}")
      if @data.include?(p)
        $LOG.debug("Which is included in @data")
        @data[p].each do |unused, binaries|
          $LOG.debug("And has entry #{binaries}")
          binaries.each do |name, binary|
#            text+= "YAML:\n"
#            text+= YAML::dump(binary)
#            text+="YAML end\n\n"
            if not binary.nil? and binary.include?("versions")
              binary["versions"].each do |version|
                text += "<tr>\n"
                text += "   <td class=\"pkg-name\">#{name}</td>\n"
                text += "   <td class=\"pkg-ver\">#{version}</td>\n"
                binary[version]["arch"].uniq!
                text += "   <td class=\"pkg-arches\">#{binary[version]["arch"].join(", ")}</td>\n"
                text += "</tr>\n"
              end # binary["versions"].each
            end # if not
          end # binaries.each
        end # @data[p]
      end # if @data
    end # @helper.each
    text
  end # def binary_rows

  def binarylist(package)
    packagelist = Array.new
    $LOG.debug("We are in binarylist now, looking for #{package}")
    $LOG.debug(YAML::dump(@data))
    package = "fakejj" if @data.has_key?("fakejj")
    @data.each_pair do |unused, work|
      $LOG.debug("Looking at #{unused} - #{work}")
      $LOG.debug(YAML::dump(work))
      work.each_pair do |name, tree|
        $LOG.debug("Looking at entry #{name} with data #{tree}")
        tree.each_pair do |paket, drop|
          $LOG.debug("Paket: #{paket}")
          packagelist << paket
        end # tree.each
      end # work.each
    end # @data.each
    $LOG.info("Debug packagelist: #{packagelist}")
    packagelist.join(" ")
  end # def binarylist

  def add_package(p, suite)
    $LOG.debug("Adding #{p} from #{suite}")
    p.sub!(/ /, "")
    if is_source?(p, suite)
      @data["#{p}"] = Hash.new
      @data["#{p}"]["binary"] = Hash.new
      @data["#{p}"]["binary"]["#{p}"] = Hash.new
      @data["#{p}"]["binary"]["#{p}"]["name"] = p
      @helper << p
    else
      if not @data["fakejj"] then
        @data["fakejj"] = Hash.new
        @data["fakejj"]["binary"] = Hash.new
      end
      @data["fakejj"]["binary"]["#{p}"] = Hash.new
      @data["fakejj"]["binary"]["#{p}"]["name"] = p
      @helper << "fakejj"
      @faked << p
    end # if is_source?
  end # add_package

  def add_binary_to_source(bin, source)
    $LOG.debug("Looking at bin #{bin}, source #{source}")
    @data["#{source}"]["binary"]["#{bin}"] = Hash.new if @data["#{source}"]["binary"]["#{bin}"].nil?
    @data["#{source}"]["binary"]["#{bin}"]["name"] = bin
  end # add_binary_to_source

  def check_source_binaries(suite)
    #  look for all binary package names for the source packages
    $LOG.debug("Checking all sources binaries in #{suite}")
    tocheck = Array.new
    @data.each do |name, entry|
      if name == "fakejj"
        $LOG.debug("Checking a fakejj package #{YAML::dump(entry)}")
        entry["binary"].each do |nam, unused|
          $LOG.debug("Now checking #{nam}")
          tocheck << nam
        end
      else
        tocheck << name
      end
      $LOG.debug("Now checking #{tocheck}")
      tocheck.each do |check|
        @db.query("SELECT DISTINCT b.package as package FROM binaries b, bin_associations ba, suite su, source s WHERE b.source = s.id AND su.id = ba.suite AND b.id = ba.bin AND s.source = '#{check}' AND su.id in (#{suite})") do |row|
          add_binary_to_source(row["package"], name)
          @helper << row["package"]
        end
      end
    end # @data.each
  end # check_source_binaries

  def check_source(suite)
    # look up information about the source itself
    $LOG.debug("Looking up information about the source")
    @data.each do |name, entry|
      next if name == "fakejj"
      @db.query("SELECT s.source, s.version, \'source\', su.suite_name, c.name, m.name FROM source s, suite su, src_associations sa, files f, files_archive_map fm, component c, maintainer m WHERE s.source = '#{name}' AND su.id = sa.suite AND s.id = sa.source AND s.file = f.id AND f.id = fm.file_id AND fm.component_id = c.id AND s.maintainer = m.id AND su.id IN (#{suite})") do |row|
        @data["#{name}"]["binary"]["#{name}"] = Hash.new if @data["#{name}"]["binary"]["#{name}"].nil?
        @data["#{name}"]["binary"]["#{name}"]["versions"] = Array.new if @data["#{name}"]["binary"]["#{name}"]["versions"].nil?
        @data["#{name}"]["binary"]["#{name}"]["versions"] << row["version"] unless @data["#{name}"]["binary"]["#{name}"]["versions"].include?(row["version"])
        @data["#{name}"]["binary"]["#{name}"][row["version"]] = Hash.new if @data["#{name}"]["binary"]["#{name}"][row["version"]].nil?
        @data["#{name}"]["binary"]["#{name}"][row["version"]]["arch"] = Array.new if @data["#{name}"]["binary"]["#{name}"][row["version"]]["arch"].nil?
        @data["#{name}"]["binary"]["#{name}"][row["version"]]["arch"] << "source"
      end # db.query
      @db.query("SELECT m.name,s.version,s.install_date FROM src_associations sa, source s JOIN fingerprint f ON (s.sig_fpr = f.id) JOIN maintainer m ON (s.maintainer = m.id) WHERE s.source = '#{name}' AND sa.source = s.id AND sa.suite IN (#{suite})") do |row|
        @data["#{name}"]["binary"]["#{name}"][row["version"]]["uploader"] = row["name"]
        @data["#{name}"]["binary"]["#{name}"][row["version"]]["uploaded"] = row["install_date"]
      end # db.query
    end # @data.each
  end # check_source

  def check_binaries(suite)
    # look up information for all the binary packages
    $LOG.debug("Lookup information about each binary")
    @data.each do |name, entry|
      if name == "fakejj"
        @faked.each do |pack|
          @db.query("SELECT b.package, b.version, a.arch_string, su.suite_name, c.name, m.name FROM binaries b, architecture a, suite su, bin_associations ba, files f, files_archive_map fm, component c, maintainer m WHERE b.package = '#{pack}' AND a.id = b.architecture AND su.id = ba.suite AND b.id = ba.bin AND b.file = f.id AND f.id = fm.file_id AND fm.component_id = c.id AND b.maintainer = m.id AND su.id IN (#{suite})") do |row|
            @data["#{name}"]["binary"]["#{row["package"]}"] = Hash.new if @data["#{name}"]["binary"]["#{row["package"]}"].nil?
            @data["#{name}"]["binary"]["#{row["package"]}"]["versions"] = Array.new if @data["#{name}"]["binary"]["#{row["package"]}"]["versions"].nil?
            @data["#{name}"]["binary"]["#{row["package"]}"]["versions"] << row["version"] unless @data["#{name}"]["binary"]["#{row["package"]}"]["versions"].include?(row["version"])
            @data["#{name}"]["binary"]["#{row["package"]}"][row["version"]] = Hash.new if @data["#{name}"]["binary"]["#{row["package"]}"][row["version"]].nil?
            @data["#{name}"]["binary"]["#{row["package"]}"][row["version"]]["arch"] = Array.new if @data["#{name}"]["binary"]["#{row["package"]}"][row["version"]]["arch"].nil?
            @data["#{name}"]["binary"]["#{row["package"]}"][row["version"]]["arch"] << row["arch_string"]
          end
        end
      else
        @helper.each do |p|
          @db.query("SELECT b.package, b.version, a.arch_string, su.suite_name, c.name, m.name FROM binaries b, architecture a, suite su, bin_associations ba, files f, files_archive_map fm, component c, maintainer m WHERE b.package = '#{p}' AND a.id = b.architecture AND su.id = ba.suite AND b.id = ba.bin AND b.file = f.id AND f.id = fm.file_id AND fm.component_id = c.id AND b.maintainer = m.id AND su.id IN (#{suite})") do |row|
            @data["#{name}"]["binary"]["#{row["package"]}"] = Hash.new if @data["#{name}"]["binary"]["#{row["package"]}"].nil?
            @data["#{name}"]["binary"]["#{row["package"]}"]["name"] = row["package"]
            @data["#{name}"]["binary"]["#{row["package"]}"]["versions"] = Array.new if @data["#{name}"]["binary"]["#{row["package"]}"]["versions"].nil?
            @data["#{name}"]["binary"]["#{row["package"]}"]["versions"] << row["version"] unless @data["#{name}"]["binary"]["#{row["package"]}"]["versions"].include?(row["version"])
            @data["#{name}"]["binary"]["#{row["package"]}"][row["version"]] = Hash.new if @data["#{name}"]["binary"]["#{row["package"]}"][row["version"]].nil?
            @data["#{name}"]["binary"]["#{row["package"]}"][row["version"]]["arch"] = Array.new if @data["#{name}"]["binary"]["#{row["package"]}"][row["version"]]["arch"].nil?
            @data["#{name}"]["binary"]["#{row["package"]}"][row["version"]]["arch"] << row["arch_string"]
          end
        end # @helper.each
      end # if name=="fakejj"
    end
  end # check_binaries

  def get_package_names(split)
    ret = Array.new
    @data.each do |name, unused|
      if name == "fakejj"
        @data["fakejj"]["binary"].each do |bin, unused|
          ret << bin
        end
      else
        ret << name
      end
    end # @data.each
    $LOG.debug("ret is #{ret.join(split)}")
    ret.join(split)
  end # get_package_name

  def get_uploader
    ret = Array.new
    @helper.uniq!
    @helper.each do |p|
      if @data.include?(p)
        @data[p].each do |unused, binaries|
          binaries.each do |name, binary|
            if not binary.nil? and binary.include?("versions")
              binary["versions"].each do |version|
                if binary[version]["arch"].include?("source")
                  ret << "Last upload: #{binary[version]["uploaded"]} with maintainer set to #{binary[version]["uploader"]}"
                end # if binary
              end # binary["versions"].each
            end # if not
          end # binaries.each
        end # @data[p]
      end # if @data
    end # @helper.each
    ret
  end # get_uploader

  def nosource?
    true if @data.include?("fakejj")
  end # nosource?

  def dump
    YAML::dump(@data)
  end #dump

  def include?(p)
    p.each do |entry|
      true if @data.include?(entry)
    end
  end # include?

  # defined to let enumerable work
  def each
    @data.each do |name, binary|
      yield name, binary
    end
  end

  private

  def is_source?(p, suite)
    t = Array.new
    @db.query("select version from source s, src_associations sa where s.source = '#{p}' and sa.source = s.id and sa.suite IN (#{suite});") do |row|
      t << row
    end
    if t.length > 0
      true
    else
      false
    end
  end # is_source?

end # class
